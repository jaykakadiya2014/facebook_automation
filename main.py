import random
import os
import subprocess
import os, winshell
from win32com.client import Dispatch
import shutil
import winreg
import time, random
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import WebElement
import asyncio
import json, threading
import pandas as pd
import requests
from requests.exceptions import ProxyError, ReadTimeout
import stem
import stem.process
from stem.control import Controller
from stem import Signal
import re
import psutil

SOCKS_PORT = 9050
def kill_process_using_port(port):
    for proc in psutil.process_iter(['pid', 'name']):
        try:
            connections = proc.connections()
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            continue

        for conn in connections:
            if conn.laddr.port == port:
                pid = proc.pid
                print(f"Killing process {pid} ({proc.name()})")
                try:
                    proc.kill()
                except:
                    pass
kill_process_using_port(SOCKS_PORT)
TOR_PATH = os.path.abspath("Tor Browser\\Browser\\TorBrowser\\Tor\\tor.exe")

# PROXIES = { 'http': 'socks5://127.0.0.1:9050', 'https': 'socks5://127.0.0.1:9050' }
PROXY = None



# function to change the Tor exit node
def change_tor_exit_node():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)

# import aiohttp
# import openpyxl
# https://mbasic.facebook.com/login/?ref=dbl&fl&login_from_aymh=1

chromedriver_path = os.path.abspath("chromedriver.exe")

ser = Service(chromedriver_path)

Chrome_UserData_path = os.path.join(os.getenv('LOCALAPPDATA'), 'Google\\Chrome\\User Data')
Chrome_profile_path = os.path.abspath("Chrome_Data\\")
Chrome_Path = None
Range_browser_open = None

excelsheet_path = os.path.abspath("Facebook_User_Data.xlsx")
# wb_obj = openpyxl.load_workbook(excelsheet_path)

class ACCOUNT:
    No: 0
    Username = ""
    Password = ""
    AppCodes = []
LIST_ACCOUNT = []

df_accounts = pd.read_excel(excelsheet_path, sheet_name='Accounts')
max_row = df_accounts.shape[0]
max_column = df_accounts.shape[1]

for x, row  in df_accounts.iterrows() :
    obj = ACCOUNT()
    obj.No = row[0]
    obj.Username = row[1]
    obj.Password = row[2]

    for y in range(3, max_column):
        try:
            if row[y] is not None and int(row[y]) > 0:
                obj.AppCodes.append(row[y])
        except:
            continue

    LIST_ACCOUNT.append(obj)


df = pd.read_excel(excelsheet_path, sheet_name='Sheet1')
max_row = df.shape[0]
max_column = df.shape[1]



class objData:
    Account = ''
    Group = ''
    Post = ''


account_data = []

for a, b, c in zip(df["Account"], df["Group"], df["Post"]):
    obj = objData()

    obj.Account = str(a)
    obj.Group = str(b)
    obj.Post = str(c)

    account_data.append(obj)

PROXIES = [{'http': 'http://104.252.131.113:3128'}]

Chrome_Data_path = os.path.abspath("Chrome_Data\\")


def find_chrome_path():
    # Check the default installation path
    default_path = r'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe'
    if os.path.isfile(default_path):
        return default_path

    default_path = r'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe'
    if os.path.isfile(default_path):
        return default_path

    # Check the registry for other installation paths
    reg_path = r'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe'
    with winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, reg_path, 0, winreg.KEY_READ) as key:
        value = winreg.QueryValueEx(key, None)[0]
        if os.path.isfile(value):
            return value

    # Chrome not found
    return None


def create_chrome_shortcut(Chrome_Path,no):
    # Find the Chrome executable path
    if Chrome_Path is None:
        Chrome_Path = find_chrome_path()

    # Create a Chrome profile directory
    profile_dir = os.path.join(Chrome_profile_path, 'UserProfile_{}'.format(str(no)))
    if not os.path.exists(profile_dir):
        os.makedirs(profile_dir)

    # Copy an existing Preferences file to the profile directory, chrome://version/
    preferences_file = os.path.join(Chrome_UserData_path, 'Default\\Preferences')
    if not os.path.exists(preferences_file):
        preferences_file = os.path.join(Chrome_UserData_path, 'Guest Profile\\Preferences')
    new_preferences_file = os.path.join(profile_dir, 'Preferences')
    shutil.copy(preferences_file, new_preferences_file)

    # Create the shortcut
    shortcut_path = os.path.join(Chrome_profile_path, f"ChromeShortCut_{str(no)}.lnk")

    # Check if the shortcut file exists
    if not os.path.exists(shortcut_path):
        # Create the shortcut
        shell = Dispatch('WScript.Shell')
        shortcut = shell.CreateShortCut(shortcut_path)
        shortcut.TargetPath = Chrome_Path
        shortcut.Arguments = "--profile-directory=\"UserProfile_" + str(no) + "\""
        shortcut.save()

    # Launch the shortcut
    # os.startfile(shortcut_path)

    return Chrome_Path


def connect_driver(no):
    try:
        
        profile_path = os.path.join(Chrome_profile_path, 'UserProfile_{}'.format(str(no)))

        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        # options.add_experimental_option("excludeSwitches", ["enable-automation"])
        # options.add_experimental_option('useAutomationExtension', False)
        # options.add_experimental_option("debuggerAddress", f"localhost:{port}")
        options.add_argument('--disable-notifications')
        options.add_argument('--disable-geolocation')
        options.add_argument("--disable-features=WebUSB")
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-popup-blocking")

        global PROXY
        if PROXY != None:
            change_tor_exit_node()
            options.add_argument('--proxy-server=%s' % PROXY)


        options.add_argument(
            'user-agent=Mozilla/5.0 (Linux; Android 11; 10011886A Build/RP1A.200720.011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.69 Safari/537.36')
        options.add_argument(
            'Accept=text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8')
        options.add_argument('Accept-Language=en-US,en;q=0.5')
        options.add_argument('Connection=keep-alive')
        options.add_argument('Upgrade-Insecure-Requests=1')
        options.add_argument('Sec-Fetch-Dest=document')
        options.add_argument('Sec-Fetch-Mode=navigate')
        options.add_argument('Sec-Fetch-Site=none')
        options.add_argument('Sec-Fetch-User=?1')
        options.add_argument('Pragma=no-cache')
        options.add_argument('Cache-Control=no-cache')

        options.add_argument("user-data-dir=" + profile_path)  # Path to your chrome profile
    
        global ser
        driver = webdriver.Chrome(service=ser, options=options)
        wait = WebDriverWait(driver, 10)

        

        return driver

    except Exception as e:
        pass


def port_fatch():
    try:
        port_list = []
        with open('chrome_data.bat', 'r') as file:
            lines = file.readlines()
            for line in lines:
                if "port" in str(line):
                    index1 = str(line).index('port=') + len('port=')
                    index2 = str(line).index(' ', index1)
                    port = line[index1:index2]
                    port_list.append(port)
        return port_list
    except Exception as e:
        return None


def create_chrome_file(li):
    try:
        # create a list of lines to write
        filename = os.path.abspath("chor.bat")
        # open a file for writing
        with open(filename, 'w') as f:
            # iterate over the lines and write each line
            for line in li:
                f.write(line + '\n')

        f.close()

        return "done"

    except Exception as e:
        return "error"


def join_group(groups_join, driver):
    try:
        if groups_join:
            for gid in groups_join:
                try:
                    driver.get(f"https://mbasic.facebook.com/groups/{gid}")
                    time.sleep(5)
                    driver.find_element(By.XPATH, "//input[@value='Join Group' and @type='submit']").click()
                    time.sleep(5)
                except Exception as e:
                    continue
        time.sleep(3)
        driver.close()
        driver.quit()
    except Exception as e:
        pass


def post_data(account_data, driver, i):
    try:
        driver.get('https://mbasic.facebook.com/groups/')
        time.sleep(3)
        try:
            ul_element = driver.find_element(by=By.XPATH, value="//h3[text()='Groups You Are In']")
        except:
            ul_element = driver.find_element(by=By.XPATH, value="//h3[text()='Groups you are in']")
        if ul_element is not None:
            perent_element = ul_element.find_element(by=By.XPATH, value="./..")
            a_tags = perent_element.find_elements(By.XPATH, ".//a[@href]")
            group_link = [a_tag.get_attribute("href") for a_tag in a_tags]
            for url in group_link:
                try:
                    driver.get(url)
                    time.sleep(3)
                    group_id = driver.find_element(By.XPATH, "//meta[@property='al:android:url']")
                    group_id = group_id.get_attribute("content").replace('fb://group/', '').strip()

                    # post data on group
                    post_list = [x.Post for x in account_data if int(x.Account) == i + 1 and x.Group == group_id]

                    for post_data in post_list:
                        try:
                            driver.get(f"https://mbasic.facebook.com/groups/{group_id}")
                            time.sleep(3)
                            text_area = driver.find_element(by=By.XPATH, value="//textarea[@name='xc_message']")
                            text_area.send_keys(post_data)
                            time.sleep(3)
                            driver.find_element(by=By.XPATH, value="//input[@name='view_post']").click()
                            time.sleep(3)
                        except Exception as e:
                            continue

                except Exception as e:
                    continue
        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass


def like_post(driver):
    try:
        driver.get('https://mbasic.facebook.com/')
        time.sleep(3)

        a_tags = driver.find_elements(by=By.XPATH, value="//a[text()='Like']")
        a_tags = [a_tag.get_attribute("href") for a_tag in a_tags]
        for url in a_tags:
            driver.execute_script(f"window.open('{url}', '_blank')")
            driver.switch_to.window(driver.window_handles[1])
            index1 = url.index('ft_ent_identifier=')
            index2 = url.index('&', index1)
            id = url[index1 + len('ft_ent_identifier='):index2]
            time.sleep(3)
            try:
                like_parent = driver.find_elements(by=By.XPATH, value=f"//div[@id='actions_{id}']")

                like_a = like_parent.find_elements(by=By.XPATH, value="//span[text()='Like']")
                perent_element = like_a.find_element(by=By.XPATH, value="./..")
                driver.get(perent_element.get_attribute("href"))
                time.sleep(3)
            except Exception as e:
                time.sleep(1)
            finally:
                driver.close()  # close the new tab
                driver.switch_to.window(driver.window_handles[0])

        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass


def accepts_requests(driver):
    try:
        driver.get('https://mbasic.facebook.com/friends/center/requests/')
        time.sleep(3)

        seemore_element = None
        perent_seemore_element = None
        
        try:
            seemore_element = driver.find_element(by=By.XPATH, value="//span[text()='See more']")
            perent_seemore_element = seemore_element.find_element(by=By.XPATH, value="./..")
        except:
            pass

        if seemore_element!=None and perent_seemore_element!=None:
            while seemore_element!=None and perent_seemore_element!=None:
                ul_elements = driver.find_elements(by=By.XPATH, value="//span[text()='Confirm']")
                for ul_element in ul_elements:
                    perent_element = ul_element.find_element(by=By.XPATH, value="./..")
                    url = perent_element.get_attribute("href")
                    driver.execute_script(f"window.open('{url}', '_blank')")
                    driver.switch_to.window(driver.window_handles[1])
                    time.sleep(3)
                    try:
                        driver.close()  # close the new tab
                        driver.switch_to.window(driver.window_handles[0])
                    except:
                        pass                        
                    
                perent_seemore_element.click()
                        
                try:
                    seemore_element = driver.find_element(by=By.XPATH, value="//span[text()='See more']")
                    perent_seemore_element = seemore_element.find_element(by=By.XPATH, value="./..")
                except:
                    seemore_element = None
                    perent_seemore_element = None

        else:
            ul_elements = driver.find_elements(by=By.XPATH, value="//span[text()='Confirm']")
            for ul_element in ul_elements:
                perent_element = ul_element.find_element(by=By.XPATH, value="./..")
                url = perent_element.get_attribute("href")
                driver.execute_script(f"window.open('{url}', '_blank')")
                driver.switch_to.window(driver.window_handles[1])
                time.sleep(3)
                try:
                    driver.close()  # close the new tab
                    driver.switch_to.window(driver.window_handles[0])
                except:
                    pass          


        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass

def send_requests(driver):
    try:
        driver.get('https://mbasic.facebook.com/friends/')
        time.sleep(3)

        seemore_element = None
        perent_seemore_element = None
        
        try:
            seemore_element = driver.find_element(by=By.XPATH, value="//span[text()='See more']")
            perent_seemore_element = seemore_element.find_element(by=By.XPATH, value="./..")
        except:
            pass

        if seemore_element!=None and perent_seemore_element!=None:
            while seemore_element!=None and perent_seemore_element!=None:
                ul_elements = driver.find_elements(by=By.XPATH, value="//span[text()='Add Friend']")
                for ul_element in ul_elements:
                    perent_element = ul_element.find_element(by=By.XPATH, value="./..")
                    url = perent_element.get_attribute("href")
                    driver.execute_script(f"window.open('{url}', '_blank')")
                    driver.switch_to.window(driver.window_handles[1])
                    time.sleep(3)
                    try:
                        driver.close()  # close the new tab
                        driver.switch_to.window(driver.window_handles[0])
                    except:
                        pass
                perent_seemore_element.click()
                        
                try:
                    seemore_element = driver.find_element(by=By.XPATH, value="//span[text()='See more']")
                    perent_seemore_element = seemore_element.find_element(by=By.XPATH, value="./..")
                except:
                    seemore_element = None
                    perent_seemore_element = None

        else:
            ul_elements = driver.find_elements(by=By.XPATH, value="//span[text()='Add Friend']")
            for ul_element in ul_elements:
                perent_element = ul_element.find_element(by=By.XPATH, value="./..")
                url = perent_element.get_attribute("href")
                driver.execute_script(f"window.open('{url}', '_blank')")
                driver.switch_to.window(driver.window_handles[1])
                time.sleep(3)
                try:
                    driver.close()  # close the new tab
                    driver.switch_to.window(driver.window_handles[0])
                except:
                    pass         


        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass

def delete_requests(driver):
    try:
        driver.get('https://mbasic.facebook.com/friends/center/requests/')
        time.sleep(3)

        seemore_element = None
        perent_seemore_element = None
        
        try:
            seemore_element = driver.find_element(by=By.XPATH, value="//span[text()='See more']")
            perent_seemore_element = seemore_element.find_element(by=By.XPATH, value="./..")
        except:
            pass

        if seemore_element!=None and perent_seemore_element!=None:
            while seemore_element!=None and perent_seemore_element!=None:
                ul_elements = driver.find_elements(by=By.XPATH, value="//span[text()='Delete request']")
                for ul_element in ul_elements:
                    perent_element = ul_element.find_element(by=By.XPATH, value="./..")
                    url = perent_element.get_attribute("href")
                    driver.execute_script(f"window.open('{url}', '_blank')")
                    driver.switch_to.window(driver.window_handles[1])
                    time.sleep(3)
                    try:
                        driver.close()  # close the new tab
                        driver.switch_to.window(driver.window_handles[0])
                    except:
                        pass                        
                    
                perent_seemore_element.click()
                        
                try:
                    seemore_element = driver.find_element(by=By.XPATH, value="//span[text()='See more']")
                    perent_seemore_element = seemore_element.find_element(by=By.XPATH, value="./..")
                except:
                    seemore_element = None
                    perent_seemore_element = None

        else:
            ul_elements = driver.find_elements(by=By.XPATH, value="//span[text()='Delete request']")
            for ul_element in ul_elements:
                perent_element = ul_element.find_element(by=By.XPATH, value="./..")
                url = perent_element.get_attribute("href")
                driver.execute_script(f"window.open('{url}', '_blank')")
                driver.switch_to.window(driver.window_handles[1])
                time.sleep(3)
                try:
                    driver.close()  # close the new tab
                    driver.switch_to.window(driver.window_handles[0])
                except:
                    pass          


        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass


def like_group_post(driver):
    try:
        driver.get('https://mbasic.facebook.com/groups/')
        time.sleep(3)
        try:
            ul_element = driver.find_element(by=By.XPATH, value="//h3[text()='Groups You Are In']")
        except:
            ul_element = driver.find_element(by=By.XPATH, value="//h3[text()='Groups you are in']")
        if ul_element is not None:
            perent_element = ul_element.find_element(by=By.XPATH, value="./..")
            a_tags = perent_element.find_elements(By.XPATH, ".//a[@href]")
            group_link = [a_tag.get_attribute("href") for a_tag in a_tags]
            for url in group_link:
                try:
                    driver.get(url)
                    time.sleep(3)
                    group_id = driver.find_element(By.XPATH, "//meta[@property='al:android:url']")
                    group_id = group_id.get_attribute("content").replace('fb://group/', '').strip()

                    a_tags = driver.find_elements(by=By.XPATH, value="//a[text()='Like']")
                    a_tags = [a_tag.get_attribute("href") for a_tag in a_tags]
                    for url in a_tags:
                        driver.execute_script(f"window.open('{url}', '_blank')")
                        driver.switch_to.window(driver.window_handles[1])
                        index1 = url.index('ft_ent_identifier=')
                        index2 = url.index('&', index1)
                        id = url[index1 + len('ft_ent_identifier='):index2]
                        time.sleep(3)
                        try:
                            like_parent = driver.find_elements(by=By.XPATH, value=f"//div[@id='actions_{id}']")

                            like_a = like_parent.find_elements(by=By.XPATH, value="//span[text()='Like']")
                            perent_element = like_a.find_element(by=By.XPATH, value="./..")
                            driver.get(perent_element.get_attribute("href"))
                            time.sleep(3)
                        except Exception as e:

                            try:
                                driver.find_elements(by=By.XPATH,
                                                     value="//*[@id=\"root\"]/table/tbody/tr/td/ul/li[1]/table/tbody/tr/td/a").click()
                                time.sleep(3)
                            except Exception as e:
                                time.sleep(1)
                        finally:
                            driver.close()  # close the new tab
                            driver.switch_to.window(driver.window_handles[0])

                except Exception as e:
                    continue

        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass


def leave_group(driver):
    try:
        driver.get('https://mbasic.facebook.com/groups/')
        time.sleep(3)
        try:
            ul_element = driver.find_element(by=By.XPATH, value="//h3[text()='Groups You Are In']")
        except:
            ul_element = driver.find_element(by=By.XPATH, value="//h3[text()='Groups you are in']")

        if ul_element is not None:
            perent_element = ul_element.find_element(by=By.XPATH, value="./..")
            a_tags = perent_element.find_elements(By.XPATH, ".//a[@href]")
            group_link = [a_tag.get_attribute("href") for a_tag in a_tags]
            for url in group_link:
                try:
                    driver.get(url)
                    time.sleep(3)
                    group_id = driver.find_element(By.XPATH, "//meta[@property='al:android:url']")
                    group_id = group_id.get_attribute("content").replace('fb://group/', '').strip()
                    driver.get(f"https://mbasic.facebook.com/group/leave/?group_id={group_id}")
                    time.sleep(3)
                    try:
                        driver.find_element(By.XPATH, "//input[@value='Leave Group']").click()
                    except:
                        driver.find_element(By.XPATH, "//input[@value='Leave group']").click()
                    time.sleep(3)
                except Exception as e:
                    continue
        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        time.sleep(1)


def comments(driver, list_comment):
    try:
        driver.get('https://mbasic.facebook.com/')
        time.sleep(3)

        a_tags = driver.find_elements(by=By.XPATH, value="//a[text()='Comment']")
        a_tags = [a_tag.get_attribute("href") for a_tag in a_tags]
        for url in a_tags:
            driver.execute_script(f"window.open('{url}', '_blank')")
            driver.switch_to.window(driver.window_handles[1])
            # index1 = url.index('ft_ent_identifier=')
            # index2 = url.index('&', index1)
            # id = url[index1 + len('ft_ent_identifier='):index2]
            time.sleep(3)
            try:
                comment_field = driver.find_element(by=By.XPATH, value="//input[@name='comment_text']")
                comment_button = driver.find_element(by=By.XPATH, value="//input[@value='Comment']")
                comment_field.send_keys(random.choice(list_comment))
                comment_button.click()
                time.sleep(3)
            except Exception as e:
                time.sleep(1)
            finally:
                driver.close()  # close the new tab
                driver.switch_to.window(driver.window_handles[0])

        time.sleep(3)
        driver.close()
        driver.quit()

    except Exception as e:
        pass


def run_bat(filename):
    try:
        # Replace "path/to/batch/file.bat" with the actual path to your batch file
        batch_file_path = os.path.abspath(filename)

        # Run the batch file
        subprocess.call([batch_file_path])

    except Exception as e:
        pass

def login_profile(user_profile, driver):
    try:
        if user_profile:
            driver.get(f"https://mbasic.facebook.com/")
            time.sleep(3)
            email_field = driver.find_element(by=By.XPATH, value="//input[@name='email']")
            password_field = driver.find_element(by=By.XPATH, value="//input[@name='pass']")
            login_button = driver.find_element(by=By.XPATH, value="//input[@name='login']")
            email_field.clear()
            password_field.clear()
            email_field.send_keys(user_profile.Username)
            password_field.send_keys(user_profile.Password)
            login_button.click()
            time.sleep(3)

            IsLogin = False
            count = 0
            while(IsLogin == False and count<=3):
                count +=1
                approvals_code_field = driver.find_element(by=By.XPATH, value="//input[@name='approvals_code']")
                submit_button = driver.find_element(by=By.XPATH, value="//input[@name='submit[Submit Code]']")
                approvals_code_field.clear()
                approvals_code_field.send_keys(random.choice(user_profile.AppCodes))
                submit_button.click()
                time.sleep(5)

                try:
                    submit_button = driver.find_element(by=By.XPATH, value="//input[@name='submit[Continue]']")
                    submit_button.click()
                    IsLogin = True
                except:
                    IsLogin = False
                    continue

            if count > 3:
                # add note in excelsheet
                time.sleep(3)

        time.sleep(3)
        driver.close()
        driver.quit()
    except Exception as e:
        pass


def finding_process_main(number_main):

    if number_main == "1":
        how_m = int(input("how many browser you want to open (ex. 10,25,50,....): "))
        for n in range(0, how_m):
            Chrome_Path = create_chrome_shortcut(Chrome_Path, n + 1)

    if number_main == "2":
        global Range_browser_open
        Range_browser_open = str("1-2")

    if number_main == "3":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            driver.get('https://mbasic.facebook.com/')
            time.sleep(5)
            
            groups_join = [item.Group for item in account_data if int(item.Account) == int(str(i + 1))]
            groups_join = list(set(groups_join))
            t2 = threading.Thread(target=join_group, args=(groups_join, driver,))
            t2.start()

    if number_main == "4":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=leave_group, args=(driver,))
            t2.start()

    if number_main == "5":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=like_post, args=(driver,))
            t2.start()

    if number_main == "6":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=like_group_post, args=(driver,))
            t2.start()

    if number_main == "7":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=post_data, args=(account_data, driver, i,))
            t2.start()

    if number_main == "8":
        list_comment = ["Test", "Test1"]
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=comments, args=(driver, list_comment,))
            t2.start()

    if number_main == "12":
        flag = False
    if number_main == "9":
        PROXY = "socks5://127.0.0.1:9050"
    if number_main == "10":
        # change the Tor exit node
        change_tor_exit_node()

    if number_main == "11":
        PROXY = None
    
    if number_main=="13":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=accepts_requests, args=(driver,))
            t2.start()

    if number_main=="14":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=send_requests, args=(driver,))
            t2.start()

    if number_main=="15":
        start = int(Range_browser_open.split('-')[0])
        end = int(Range_browser_open.split('-')[1])
        for i in range(start, end + 1):
            driver = connect_driver(i)
            t2 = threading.Thread(target=delete_requests, args=(driver,))
            t2.start()

    return "true"

